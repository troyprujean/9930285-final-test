/*---------------------------------------------\
|   Client: EZ Van Hire                        |
|   Title: Van management system               |
|   Version: 1.0 beta                          |
|   Purpose: Manage vans more effectively      |
|   Environment: Chrome browser                |
|   Author: Troy Prujean                       |
|   Date: 14/06/2018                           |
\---------------------------------------------*/

// Definition of the Van class
class Van {
    // Constructor for the Van class properties using the default get and set methods and including parameters for pre loading arguments into instantiations
    constructor (vID, vCapacity, vLicense, vCost, vInsurance) {
        this.VanID = vID;
        this.Capacity = vCapacity;
        this.License = vLicense;
        this.CostPerDay = vCost;
        this.Insurance = vInsurance;
    }

    // Default getter and setters for the Van class properties
    get VanID () { return this.vanIDCode; } set VanID (value) { this.vanIDCode = value; }

    get Capacity () { return this.vanCapacity; } set Capacity (value) { this.vanCapacity = value; }

    get License () { return this.licenseType; } set License (value) { this.licenseType = value; }

    get CostPerDay () { return this.vanCostPerDay; } set CostPerDay (value) { this.vanCostPerDay = value; }
    
    get Insurance () { return this.insuranceCostPerDay; } set Insurance (value) { this.insuranceCostPerDay = value; }

    // Function to return the total cost of the van hire per day (van cost per day + insurance cost per day)
    totalCost () {
        return this.vanCostPerDay + this.insuranceCostPerDay;
    }
}

// Main program function that runs upon user clicking the submit button
function Submit () {
    // Instantiation of a new Van object
    let van1 = new Van ();

    // Using the set functions to assign user input values from the form fields to the Van class properties
    van1.VanID = document.getElementById (`vanIDCode`).value;
    van1.Capacity = Number (document.getElementById (`vanLoadCapacity`).value);
    van1.License = document.getElementById (`vanLicenseType`).value;;
    van1.CostPerDay = Number (document.getElementById (`vanCostPerDay`).value);
    van1.Insurance = Number (document.getElementById (`vanInsuranceCostPerDay`).value);

    // Using the get functions to output the Van class properties to the HTML div containers
    document.getElementById (`output1`).innerHTML = "Van details below:";
    document.getElementById (`output2`).innerHTML = `Van ID: <b>${van1.VanID}</b>`;
    document.getElementById (`output3`).innerHTML = `Load Capacity (m<sup>3</sup>): <b>${van1.Capacity}m<sup>3</sup</b>`;
    document.getElementById (`output4`).innerHTML = `License Type: <b>${van1.License}</b>`;
    document.getElementById (`output5`).innerHTML = `Hire Cost Per Day: <b>$${van1.CostPerDay.toFixed(2)}</b>`;
    document.getElementById (`output6`).innerHTML = `Insurance Per Day: <b>$${van1.Insurance.toFixed(2)}</b>`;
    document.getElementById (`output7`).innerHTML = `------------------------------------------`;
    // Using the totalCost function to output the total cost of the van hire per day
    document.getElementById (`output8`).innerHTML = `Total Hire Cost: <b>$${van1.totalCost().toFixed(2)}</b>`;

    // Using the get functions to output the Van class properties to console
    console.log ("EZ Van Hires' van management system");
    console.log ("Van details below:");
    console.log ("-----------------------------------");
    console.log (`Van ID                : ${van1.VanID}`);
    console.log (`Load Capacity (m^3)   : ${van1.Capacity}m^3`);
    console.log (`License Type          : ${van1.License}`);
    console.log (`Hire Cost Per Day     : $${van1.CostPerDay.toFixed(2)}`);
    console.log (`Insurance Per Day     : $${van1.Insurance.toFixed(2)}`);
    // Using the totalCost function to output the total cost of the van hire per day
    console.log (`Total Hire Cost       : $${van1.totalCost().toFixed(2)}`)
}